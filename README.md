Inspired by [the Advanced Command Line PeepCode screencast](https://peepcode.com/products/advanced-command-line).

To set things up:

1. `brew install git bash-completion`
1. Move this project's folder to `~/bin/dotfiles`.
2. Run `rake install` from inside that folder.

[More information about configuring Git.](https://gist.github.com/2722934)
